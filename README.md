# Simulation of the Deva calorimeter

Author and maintainer: [P. Monti-Guarnieri](mailto:pmontiguarnieri@studenti.uninsubria.it)

# Content

This code is used to simulate the energy deposit inside the DEVA calorimeter in a beamtest (performed at the CERN PS).

**More info about how this simulation works are given in the [Documentation](Documentation/doc_simulation) folder**.

# A short guide on how to install and use Geant4 on WSL/CERN VM

For more details please see the [Documentation](Documentation/G4installation). Note that here the Geant4 release used is the usual 11.0.2; however, the simulation was originally developed within Geant4 11.1.1 and it works fine even there.

# Quick commands and memo

- **Run for grahics**: start XLaunch -> Multiple windows -> Start no client -> Uncheck "Native opengl" and check "Disable access control"; then
```bash
./deva2022
/control/execute macros/run_forgraphics.mac
```

- **Run in a given mode, for physics** (assuming Geant4 ver. 11.0.2, and assuming you work in local (for VM see the comandi.txt file)):
```bash
cd CERN2022-build
source ~/Geant4.11.0.2/geant4-11.0.2-install/bin/geant4.sh
cmake -DGeant4_DIR=~/Geant4.11.0.2/geant4-11.0.2-install/lib/Geant4-11.0.2/ ../CERN2022 && make -j4
./deva2022 macros/run_forphysics.mac
```

- **Multi-run (long statistics, both amorphous and axial)**:
```bash
cd CERN2022-build
../mastercall.sh
```