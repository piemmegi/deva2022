# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# The aim of this script is to move the files produces as output of the deva2022 simulation
# from the native folder to a safer location. 
# Note: NEVER CALL THIS SCRIPT ALONE! It is called inside the mastercall.sh script, so check
# that out instead of this.
#
# ==========================================================================================
#                            IMPORT OF THE EXTERNAL PYTHON MODULES
# ==========================================================================================
import os
import sys
import shutil

# ==========================================================================================
#                                         INPUTS
# ==========================================================================================
# Define some useful paths
thisfolder = os.path.dirname(os.path.abspath(__file__))   # Simulation folder
outfolder = thisfolder + "/Outputfiles/"                  # Folder where the output files will be stored

# Define the names of the output file (ROOT)
G4outputname = 'outfile.root'                                      # Initial name of the output file
currname = thisfolder + '/CERN2022-build/out_data/' + G4outputname # Complete version of the initial name of the output file
copyname = outfolder + G4outputname                                # Name of the output file, after copy in the output folder
newoutname = outfolder + "tbeamdata_" + sys.argv[1] + "MeV_numprt" + sys.argv[2] + ".root"    # New name of the output file (in the correct output folder)

# Define the names of the output file (meshgrid)
G4outputname_mesh = 'meshdata.csv'                                  # Initial name of the output file
currname_mesh = thisfolder + '/CERN2022-build/' + G4outputname_mesh # Complete version of the initial name of the output file
copyname_mesh = outfolder + G4outputname_mesh                       # Name of the output file, after copy in the output folder
newoutname_mesh = outfolder + "meshdata_" + sys.argv[1] + "MeV_numprt" + sys.argv[2] + ".csv" # New name of the output file (in the correct output folder)

# ==========================================================================================
#                               MOVING AND RENAMING THE OUTPUT FILE
# ==========================================================================================
# Once the simulation has ended, copy the output file in the correct folder, rename it
# and remove the trailing file
shutil.copy(currname,outfolder)
os.rename(currname,newoutname)
os.remove(copyname)

# ==========================================================================================
#                               MOVING AND RENAMING THE MESHGRID FILE
# ==========================================================================================
# Once the simulation has ended, copy the output file in the correct folder, rename it
# and remove the trailing file
shutil.copy(currname_mesh,outfolder)
os.rename(currname_mesh,newoutname_mesh)
os.remove(copyname_mesh)

# Close the script
print(f"Python task completed! \n")