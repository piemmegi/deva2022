#include <G4SystemOfUnits.hh>
#include <G4String.hh>

#include "RunAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "DetectorConstruction.hh"

#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4AccumulableManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

using namespace std;

// RunAction, class created at run start, with RunAction::EndOfRunAction executed at the end of the run

RunAction::RunAction() :  G4UserRunAction()
{
    // Load the analysis manager for data output
    G4AnalysisManager* analysis = G4AnalysisManager::Instance();
    analysis->SetVerboseLevel(1);  // set analysis manager verbosity here
  
    // Set progressive printing every X events
	G4int NumofPrintProgress = 250;
    G4RunManager::GetRunManager()->SetPrintProgress(NumofPrintProgress);
    
    // Create output ntuple
    analysis->SetNtupleMerging(true); // Note: merging ntuples is available only with Root output
    analysis->SetFirstNtupleId(0);
    analysis->CreateNtuple("outData", "output data");

    // Create the ntuple columns. Note: the hit positions are measured in cm
    // and the energy deposits are measured in GeV.
	
    analysis->CreateNtupleDColumn("NEvent");                  //  00 [adim.]
    analysis->CreateNtupleDColumn("Tracker_NHit_X_0");        //  01 [adim.]
    analysis->CreateNtupleDColumn("Tracker_NHit_Y_0");        //  02 [adim.]
    analysis->CreateNtupleDColumn("Tracker_NHit_X_1");        //  03 [adim.]
    analysis->CreateNtupleDColumn("Tracker_NHit_Y_1");        //  04 [adim.]
    analysis->CreateNtupleDColumn("Tracker_X_0");             //  05 [cm]
    analysis->CreateNtupleDColumn("Tracker_Y_0");             //  06 [cm]
    analysis->CreateNtupleDColumn("Tracker_X_1");             //  07 [cm]
    analysis->CreateNtupleDColumn("Tracker_Y_1");             //  08 [cm]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_00");     //  09 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_00");    //  10 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_01");     //  11 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_01");    //  12 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_02");     //  13 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_02");    //  14 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_03");     //  15 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_03");    //  16 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_04");     //  17 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_04");    //  18 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_05");     //  19 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_05");    //  20 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_06");     //  21 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_06");    //  22 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_07");     //  23 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_07");    //  24 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_08");     //  25 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_08");    //  26 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_09");     //  27 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_09");    //  28 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_10");     //  29 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_10");    //  30 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Active_11");     //  31 [GeV]
    analysis->CreateNtupleDColumn("Deva_EDep_Passive_11");    //  32 [GeV]
    analysis->CreateNtupleDColumn("Cherenkov_EDep");          //  33 [GeV]
    analysis->CreateNtupleDColumn("MC_EDep");                 //  34 [GeV]
    analysis->FinishNtuple(0);
}

RunAction::~RunAction()
{}

void RunAction::BeginOfRunAction(const G4Run* run)
{
    // Create the analysis manager.
    auto analysis = G4AnalysisManager::Instance();

    // Define the name of the run file with a fixed format: 4 numbers
    // (e.g., 0001, 0002, 0010, etc). The number to be set is simply the run ID.

    G4int RunNumber = G4RunManager::GetRunManager()->GetCurrentRun()->GetRunID();

    std::ostringstream ss;
    ss << std::setw(4) << std::setfill('0') << RunNumber;
    std::string RunString = ss.str();
    G4String fileName = "./out_data/tbeamdata" + RunString + ".root";

    // Open the output file of the given run
    analysis->OpenFile(fileName);

    // Give a printout of the run which is beginning
    if (IsMaster()) {
      G4cout
       << G4endl
       << "--------------------Begin of Global Run-----------------------";
    }
    else {
        //G4int ithread = G4Threading::G4GetThreadId();

        G4cout
         << G4endl
         << "--------------------Begin of Local Run------------------------"
         << G4endl;
    }
}

void RunAction::EndOfRunAction(const G4Run* run)
{
    // Retrieve the number of events produced in the run
    G4int nofEvents = run->GetNumberOfEvent();
    if (nofEvents == 0) return;

    if (IsMaster())
    {
        G4cout << "-----" << G4endl;
        G4cout << "| RunAction.cc: end of run --> generated events: " << nofEvents << G4endl;
        G4cout << "-----" << G4endl;
    }

    // Write output file & close it
    G4AnalysisManager* analysis = G4AnalysisManager::Instance();
    analysis->Write();
    analysis->CloseFile();
}