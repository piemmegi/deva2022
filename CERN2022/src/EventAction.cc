#include <G4SystemOfUnits.hh>
#include <G4SDManager.hh>
#include <G4THitsMap.hh>
#include <G4Event.hh>

#include "CustomHit.hh"
#include "EventAction.hh"
#include "RunAction.hh"

#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"

using namespace std;

// EventAction::EndOfEventAction, executed at the end of each event
void EventAction::EndOfEventAction(const G4Event* event)
{
    // Load the sensitive detector manager (set verbosity in DetectorConstruction.cc)
    G4SDManager* sdm = G4SDManager::GetSDMpointer();
	
    // Load the analysis manager for data output (set verbosity in RunAction.cc)
    G4AnalysisManager* analysis = G4AnalysisManager::Instance();

    // Get the set of all the data collections for the current event. If the set is
    // empty, then exit the function.
    G4HCofThisEvent* hcofEvent = event->GetHCofThisEvent();
    if(!hcofEvent) return;
	
    // If there are hits, process them. Specifically, start by getting each collection separately
    // Note to self: the syntax for the argument of GetCollectionID is always "NameofSD/NameofFunction"
    G4int fPhCalEDepId_Active00 = sdm->GetCollectionID("PhCalTestActive_00_SD/VolumeEDep");    // Deva
    G4int fPhCalEDepId_Passive00 = sdm->GetCollectionID("PhCalTestPassive_00_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active01 = sdm->GetCollectionID("PhCalTestActive_01_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive01 = sdm->GetCollectionID("PhCalTestPassive_01_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active02 = sdm->GetCollectionID("PhCalTestActive_02_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive02 = sdm->GetCollectionID("PhCalTestPassive_02_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active03 = sdm->GetCollectionID("PhCalTestActive_03_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive03 = sdm->GetCollectionID("PhCalTestPassive_03_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active04 = sdm->GetCollectionID("PhCalTestActive_04_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive04 = sdm->GetCollectionID("PhCalTestPassive_04_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active05 = sdm->GetCollectionID("PhCalTestActive_05_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive05 = sdm->GetCollectionID("PhCalTestPassive_05_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active06 = sdm->GetCollectionID("PhCalTestActive_06_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive06 = sdm->GetCollectionID("PhCalTestPassive_06_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active07 = sdm->GetCollectionID("PhCalTestActive_07_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive07 = sdm->GetCollectionID("PhCalTestPassive_07_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active08 = sdm->GetCollectionID("PhCalTestActive_08_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive08 = sdm->GetCollectionID("PhCalTestPassive_08_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active09 = sdm->GetCollectionID("PhCalTestActive_09_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive09 = sdm->GetCollectionID("PhCalTestPassive_09_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active10 = sdm->GetCollectionID("PhCalTestActive_10_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive10 = sdm->GetCollectionID("PhCalTestPassive_10_SD/VolumeEDep");  //
    G4int fPhCalEDepId_Active11 = sdm->GetCollectionID("PhCalTestActive_11_SD/VolumeEDep");    //
    G4int fPhCalEDepId_Passive11 = sdm->GetCollectionID("PhCalTestPassive_11_SD/VolumeEDep");  //
    
    G4int fChId = sdm->GetCollectionID("Cherenkov_SD/VolumeEDep");               // Cherenkov
    G4int fMCId = sdm->GetCollectionID("MC_SD/VolumeEDep");                      // Scintillator
    
    G4int fTrackerId0_0 = sdm->GetCollectionID("Tracker_SD_0_0/VolumeTracking");  // tracking module 0 -- 0th
    G4int fTrackerId0_1 = sdm->GetCollectionID("Tracker_SD_0_1/VolumeTracking");  // tracking module 0 -- 1st
    G4int fTrackerId1_0 = sdm->GetCollectionID("Tracker_SD_1_0/VolumeTracking");  // tracking module 1 -- 0th
    G4int fTrackerId1_1 = sdm->GetCollectionID("Tracker_SD_1_1/VolumeTracking");  // tracking module 1 -- 1st
    
    // Cast the outputs in a more appropriate form
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_00 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active00));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_00 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive00));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_01 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active01));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_01 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive01));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_02 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active02));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_02 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive02));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_03 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active03));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_03 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive03));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_04 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active04));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_04 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive04));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_05 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active05));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_05 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive05));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_06 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active06));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_06 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive06));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_07 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active07));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_07 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive07));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_08 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active08));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_08 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive08));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_09 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active09));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_09 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive09));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_10 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active10));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_10 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive10));
    VolumeEDepHitsCollection* hitCollectionPhCal_Active_11 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Active11));
    VolumeEDepHitsCollection* hitCollectionPhCal_Passive_11 = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_Passive11));

    VolumeEDepHitsCollection* hitCollectionCh = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fChId));
    VolumeEDepHitsCollection* hitCollectionMC = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fMCId));
    
    VolumeTrackingHitsCollection* hitCollectionTracker0_0 = dynamic_cast<VolumeTrackingHitsCollection*>(hcofEvent->GetHC(fTrackerId0_0));
    VolumeTrackingHitsCollection* hitCollectionTracker0_1 = dynamic_cast<VolumeTrackingHitsCollection*>(hcofEvent->GetHC(fTrackerId0_1));
    VolumeTrackingHitsCollection* hitCollectionTracker1_0 = dynamic_cast<VolumeTrackingHitsCollection*>(hcofEvent->GetHC(fTrackerId1_0));
    VolumeTrackingHitsCollection* hitCollectionTracker1_1 = dynamic_cast<VolumeTrackingHitsCollection*>(hcofEvent->GetHC(fTrackerId1_1));
	
    // Compute the event number and save it to the output ntuple
    if ((hitCollectionTracker0_0) || (hitCollectionTracker0_1))
    {analysis->FillNtupleDColumn(0, 0, event->GetEventID());}
	
    // ============================================================================
    //                               SILICON TRACKERS
    // ============================================================================

    // get tracking system data collection
    // 1 hit per particle, per step & per tracking plane
    // --> consider only the hits whose energy deposit is over a given threshold
    // --> for hits belonging to the same particle, compute the mean transverse position hit
    // --> repeat for all the different particles and treat each one separately
    // --> however, the final value given in x/y will be the average of ALL the particles
    G4double thresholdTrackerEDep = 50 * keV;     // Given by Him
		
    // --> module 0 -- 0th (Chamber 1, first plane)
    if (hitCollectionTracker0_0)
    {
        // Preinitialize to default values, like the DAQ system
        G4int lastTrackId0_0 = -1;
        G4int NStep0_0 = 1;
        G4int NHits0_0 = 0;
        G4double horsa0 = -9999.0*cm;

        for (auto hit: *hitCollectionTracker0_0->GetVector())
        {
            // The iteration is over all the registered hits (several hits * several particles)
            if (hit->GetEDep()>thresholdTrackerEDep)
            {
                // If the energy loss of the hit is beyond threshold, add the primary particle 
                // to the number of particles which crossed the active volume (do it only 1 time)
                // Moreover, add the hit coordinates in x and y for the average value calculation

                if(hit->GetTrackId() != lastTrackId0_0)
                {
                    NHits0_0+=1;
                    NStep0_0=1;
                    horsa0=hit->GetX()[0];
                }
                else
                {
                    NStep0_0+=1;
                    horsa0+=hit->GetX()[0];
                }
                lastTrackId0_0 = hit->GetTrackId();
            }
        }
        // The position measured in this event will be the average value of the positions
        // recorded for each particle with an energy loss beyond threshold.
        horsa0 = horsa0 / NStep0_0;
        analysis->FillNtupleDColumn(0, 1, NHits0_0);
        analysis->FillNtupleDColumn(0, 5, horsa0 / cm);
    }
    else
    {
        // Event is crap!
        analysis->FillNtupleDColumn(0, 1, 0);
        analysis->FillNtupleDColumn(0, 5, -9999.0 / cm);
    }
	
    // --> module 0 -- 1st (Chamber 1, second plane)
    // (See module 0 for comments on the code)
    if (hitCollectionTracker0_1)
    {
        G4int lastTrackId0_1 = -1;
        G4int NStep0_1 = 1;
        G4int NHits0_1 = 0;
        G4double versa0 = -9999.0*cm;
        for (auto hit: *hitCollectionTracker0_1->GetVector())
        {
            if (hit->GetEDep()>thresholdTrackerEDep)
            {
                if(hit->GetTrackId() != lastTrackId0_1)
                {
                    NHits0_1+=1;
                    NStep0_1=1;
                    versa0=hit->GetX()[1];
                }
                else
                {
                    NStep0_1+=1;
                    versa0+=hit->GetX()[1];
                }
                lastTrackId0_1 = hit->GetTrackId();
            }
        }
        versa0 = versa0 / NStep0_1;
        analysis->FillNtupleDColumn(0, 2, NHits0_1);
        analysis->FillNtupleDColumn(0, 6, versa0 / cm);
    }
    else
    {
        analysis->FillNtupleDColumn(0, 2, 0);
        analysis->FillNtupleDColumn(0, 6, -9999.0 / cm);
    }
	
    // --> module 0 -- 0th (Chamber 2, first plane)
    // (See module 0 for comments on the code)
    if (hitCollectionTracker1_0)
    {
        G4int lastTrackId1_0 = -1;
        G4int NStep1_0 = 1;
        G4int NHits1_0 = 0;
        G4double horsa1 = -9999.0*cm;
        for (auto hit: *hitCollectionTracker1_0->GetVector())
        {
            if (hit->GetEDep()>thresholdTrackerEDep)
            {
                if(hit->GetTrackId() != lastTrackId1_0)
                {
                    NHits1_0+=1;
                    NStep1_0=1;
                    horsa1=hit->GetX()[0];
                }
                else
                {
                    NStep1_0+=1;
                    horsa1+=hit->GetX()[0];
                }
                lastTrackId1_0 = hit->GetTrackId();
            }
        }
        horsa1 = horsa1 / NStep1_0;
        analysis->FillNtupleDColumn(0, 3, NHits1_0);
        analysis->FillNtupleDColumn(0, 7, horsa1 / cm);
    }
    else
    {
        analysis->FillNtupleDColumn(0, 3, 0);
        analysis->FillNtupleDColumn(0, 7, -9999.0 / cm);
    }
	
    // --> module 1 -- 1st (Chamber 2, second plane)
    // (See module 0 for comments on the code)
    if (hitCollectionTracker1_1)
    {
        G4int lastTrackId1_1 = -1;
        G4int NStep1_1 = 1;
        G4int NHits1_1 = 0;
        G4double versa1 = -9999.0*cm;
        for (auto hit: *hitCollectionTracker1_1->GetVector())
        {
            if (hit->GetEDep()>thresholdTrackerEDep)
            {
                if(hit->GetTrackId() != lastTrackId1_1)
                {
                    NHits1_1+=1;
                    NStep1_1=1;
                    versa1=hit->GetX()[1];
                }
                else
                {
                    NStep1_1+=1;
                    versa1+=hit->GetX()[1];
                }
                lastTrackId1_1 = hit->GetTrackId();
            }
        }
        versa1 = versa1 / NStep1_1;
        analysis->FillNtupleDColumn(0, 4, NHits1_1);
        analysis->FillNtupleDColumn(0, 8, versa1 / cm);
    }
    else
    {
        analysis->FillNtupleDColumn(0, 4, 0);
        analysis->FillNtupleDColumn(0, 8, -9999.0 / cm);
    }

    // ============================================================================
    //                           CALORIMETERS
    // ============================================================================
	
    // Get Deva calorimeter data collection
    // Note: 1 hit per particle & per step --> sum everything for the current event
	G4int colPhCalEDep = 9;
    if (hitCollectionPhCal_Active_00)
    {
        G4double PhCalEDepTot_Active_00 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_00->GetVector())
        {PhCalEDepTot_Active_00 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+0, PhCalEDepTot_Active_00 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+0, 0.0);}

    if (hitCollectionPhCal_Passive_00)
    {
        G4double PhCalEDepTot_Passive_00 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_00->GetVector())
        {PhCalEDepTot_Passive_00 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+1, PhCalEDepTot_Passive_00 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+1, 0.0);}

    if (hitCollectionPhCal_Active_01)
    {
        G4double PhCalEDepTot_Active_01 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_01->GetVector())
        {PhCalEDepTot_Active_01 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+2, PhCalEDepTot_Active_01 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+2, 0.0);}

    if (hitCollectionPhCal_Passive_01)
    {
        G4double PhCalEDepTot_Passive_01 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_01->GetVector())
        {PhCalEDepTot_Passive_01 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+3, PhCalEDepTot_Passive_01 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+3, 0.0);}

    if (hitCollectionPhCal_Active_02)
    {
        G4double PhCalEDepTot_Active_02 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_02->GetVector())
        {PhCalEDepTot_Active_02 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+4, PhCalEDepTot_Active_02 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+4, 0.0);}

    if (hitCollectionPhCal_Passive_02)
    {
        G4double PhCalEDepTot_Passive_02 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_02->GetVector())
        {PhCalEDepTot_Passive_02 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+5, PhCalEDepTot_Passive_02 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+5, 0.0);}

    if (hitCollectionPhCal_Active_03)
    {
        G4double PhCalEDepTot_Active_03 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_03->GetVector())
        {PhCalEDepTot_Active_03 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+6, PhCalEDepTot_Active_03 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+6, 0.0);}

    if (hitCollectionPhCal_Passive_03)
    {
        G4double PhCalEDepTot_Passive_03 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_03->GetVector())
        {PhCalEDepTot_Passive_03 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+7, PhCalEDepTot_Passive_03 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+7, 0.0);}

    if (hitCollectionPhCal_Active_04)
    {
        G4double PhCalEDepTot_Active_04 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_04->GetVector())
        {PhCalEDepTot_Active_04 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+8, PhCalEDepTot_Active_04 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+8, 0.0);}

    if (hitCollectionPhCal_Passive_04)
    {
        G4double PhCalEDepTot_Passive_04 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_04->GetVector())
        {PhCalEDepTot_Passive_04 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+9, PhCalEDepTot_Passive_04 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+9, 0.0);}

    if (hitCollectionPhCal_Active_05)
    {
        G4double PhCalEDepTot_Active_05 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_05->GetVector())
        {PhCalEDepTot_Active_05 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+10, PhCalEDepTot_Active_05 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+10, 0.0);}

    if (hitCollectionPhCal_Passive_05)
    {
        G4double PhCalEDepTot_Passive_05 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_05->GetVector())
        {PhCalEDepTot_Passive_05 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+11, PhCalEDepTot_Passive_05 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+11, 0.0);}

    if (hitCollectionPhCal_Active_06)
    {
        G4double PhCalEDepTot_Active_06 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_06->GetVector())
        {PhCalEDepTot_Active_06 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+12, PhCalEDepTot_Active_06 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+12, 0.0);}

    if (hitCollectionPhCal_Passive_06)
    {
        G4double PhCalEDepTot_Passive_06 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_06->GetVector())
        {PhCalEDepTot_Passive_06 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+13, PhCalEDepTot_Passive_06 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+13, 0.0);}

    if (hitCollectionPhCal_Active_07)
    {
        G4double PhCalEDepTot_Active_07 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_07->GetVector())
        {PhCalEDepTot_Active_07 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+14, PhCalEDepTot_Active_07 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+14, 0.0);}

    if (hitCollectionPhCal_Passive_07)
    {
        G4double PhCalEDepTot_Passive_07 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_07->GetVector())
        {PhCalEDepTot_Passive_07 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+15, PhCalEDepTot_Passive_07 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+15, 0.0);}

    if (hitCollectionPhCal_Active_08)
    {
        G4double PhCalEDepTot_Active_08 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_08->GetVector())
        {PhCalEDepTot_Active_08 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+16, PhCalEDepTot_Active_08 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+16, 0.0);}

    if (hitCollectionPhCal_Passive_08)
    {
        G4double PhCalEDepTot_Passive_08 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_08->GetVector())
        {PhCalEDepTot_Passive_08 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+17, PhCalEDepTot_Passive_08 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+17, 0.0);}

    if (hitCollectionPhCal_Active_09)
    {
        G4double PhCalEDepTot_Active_09 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_09->GetVector())
        {PhCalEDepTot_Active_09 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+18, PhCalEDepTot_Active_09 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+18, 0.0);}

    if (hitCollectionPhCal_Passive_09)
    {
        G4double PhCalEDepTot_Passive_09 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_09->GetVector())
        {PhCalEDepTot_Passive_09 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+19, PhCalEDepTot_Passive_09 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+19, 0.0);}

    if (hitCollectionPhCal_Active_10)
    {
        G4double PhCalEDepTot_Active_10 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_10->GetVector())
        {PhCalEDepTot_Active_10 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+20, PhCalEDepTot_Active_10 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+20, 0.0);}

    if (hitCollectionPhCal_Passive_10)
    {
        G4double PhCalEDepTot_Passive_10 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_10->GetVector())
        {PhCalEDepTot_Passive_10 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+21, PhCalEDepTot_Passive_10 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+21, 0.0);}

    if (hitCollectionPhCal_Active_11)
    {
        G4double PhCalEDepTot_Active_11 = 0.0;
        for (auto hit: *hitCollectionPhCal_Active_11->GetVector())
        {PhCalEDepTot_Active_11 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+22, PhCalEDepTot_Active_11 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+22, 0.0);}

    if (hitCollectionPhCal_Passive_11)
    {
        G4double PhCalEDepTot_Passive_11 = 0.0;
        for (auto hit: *hitCollectionPhCal_Passive_11->GetVector())
        {PhCalEDepTot_Passive_11 += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colPhCalEDep+23, PhCalEDepTot_Passive_11 / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colPhCalEDep+23, 0.0);}

    // ============================================================================
    //                              SCINTILLATORS
    // ============================================================================
    // Get Cherenkov 
	G4int colChEDep = 33;
    if (hitCollectionCh)
    {
        G4double PhChTot= 0.0;
        for (auto hit: *hitCollectionCh->GetVector())
        {PhChTot += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colChEDep, PhChTot / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colChEDep, 0.0);}

    // Get MC data collection
    G4int colMCEDep = 34;
    if (hitCollectionMC)
    {
        G4double PhMCTot= 0.0;
        for (auto hit: *hitCollectionMC->GetVector())
        {PhMCTot += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, colMCEDep, PhMCTot / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, colMCEDep, 0.0);}

    // Add the line for the new event to ntuple
    analysis->AddNtupleRow(0);
}
