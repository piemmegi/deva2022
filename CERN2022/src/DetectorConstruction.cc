// Standard includes
#include <G4SystemOfUnits.hh>
#include <G4LogicalVolume.hh>
#include <G4PVPlacement.hh>
#include <G4NistManager.hh>
#include <G4SystemOfUnits.hh>
#include <G4VisAttributes.hh>
#include <G4SDManager.hh>
#include <G4LogicalVolumeStore.hh>
#include <G4UniformMagField.hh>
#include <G4FieldManager.hh>
#include <G4TransportationManager.hh>
#include <G4ChordFinder.hh>
#include <G4MultiFunctionalDetector.hh>
#include <G4Box.hh>

#include "DetectorConstruction.hh"
#include "CustomSD.hh"
#include "G4SubtractionSolid.hh"

DetectorConstruction::DetectorConstruction()
{}  

// DetectorConstruction::Construct, i.e. where the setup geometry is implemented
G4VPhysicalVolume* DetectorConstruction::Construct()
{
    // ============================================================================
    //                                 MATERIALS DEFINITION
    // ============================================================================
    // Retrieve the NIST database for materials
    G4NistManager* nist = G4NistManager::Instance();
    
    // Create the colors used in the visualization
    //G4VisAttributes* grey = new G4VisAttributes(true, G4Colour::Grey());
    G4VisAttributes* cyan = new G4VisAttributes(true, G4Colour::Cyan());
	G4VisAttributes* green = new G4VisAttributes(true, G4Colour::Green());
	//G4VisAttributes* white = new G4VisAttributes(true, G4Colour::White());
	//G4VisAttributes* invisible = new G4VisAttributes(false);
	G4VisAttributes* brown = new G4VisAttributes(true, G4Colour::Brown());
    //G4VisAttributes* blue = new G4VisAttributes(true, G4Colour::Blue());

    // Create the most important materials
    G4Material* air = nist->FindOrBuildMaterial("G4_AIR");      // air
    G4Material* silicon = nist->FindOrBuildMaterial("G4_Si");   // silicon
    G4Material* lead = nist->FindOrBuildMaterial("G4_Pb");      // lead
	G4Material* plastic = nist->FindOrBuildMaterial("G4_POLYSTYRENE"); // polystirene, for scintillators 
    //G4Material* copper= nist->FindOrBuildMaterial("G4_Cu");     // copper
    G4Material* co2 = nist->FindOrBuildMaterial("G4_CARBON_DIOXIDE");  // CO2

    // world
    G4double worldSizeX = 10 * m;
    G4double worldSizeY = 10 * m;
    G4double worldSizeZ = 80 * m;
    G4VSolid* worldBox = new G4Box("World_Solid", worldSizeX / 2, worldSizeY / 2, worldSizeZ / 2);

    G4LogicalVolume* worldLog = new G4LogicalVolume(worldBox, air, "World_Logical");
    G4VisAttributes* visAttrWorld = new G4VisAttributes();
    visAttrWorld->SetVisibility(false);
    worldLog->SetVisAttributes(visAttrWorld);

    G4VPhysicalVolume* worldPhys = new G4PVPlacement(nullptr, {}, worldLog, "World", nullptr, false, 0);
	
    // ============================================================================
    //                                 DISTANCES
    // ============================================================================
    // Define the detectors positions along the beam axis (z)
    // (ALL THE DETECTORS)
    G4double zCherenkov = 0 * cm;            // Cherenkov center
    G4double zMC = 231.0 * cm;               // Scintillator
    G4double zChamber1 = 245.5 * cm;         // Chamber 1 longitudinal center
    G4double zChamber2 = 313.5 * cm;         // Chamber 2 longitudinal center
    G4double zPhCaloFront = 353.5 * cm;      // Deva Calorimeter

    // ============================================================================
    //                                BASIC PLACEMENTS
    // ============================================================================
    // Place the Cherenkov counter
    G4Box* Ch_box = new G4Box("Ch_physical",10*cm/2, 10*cm/2, 5*mm/2); // Dimensions TBC
    G4LogicalVolume* fLogicCh = new G4LogicalVolume(Ch_box, co2,"Ch_logical");
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCherenkov), fLogicCh, "Ch", worldLog, false, 0);

    // Place the Multiplicity Counter
    G4Box* MC_box = new G4Box("MC_physical",10*cm/2, 10*cm/2, 4.5*cm/2); // Dimensions TBC
    G4LogicalVolume* fLogicMC = new G4LogicalVolume(MC_box, plastic, "MC_logical");
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zMC), fLogicMC, "MC", worldLog, false, 0);

    // Place the silicon trackers
    G4double trackerSiliBigThickness = 410 * um;  // Chambers
    G4double trackerSiliBigWidth = 384 * 242 * um;
    G4double trackerSiliBigHeight = 384 * 242 * um;
    G4VSolid* trackerSiliBigBox = new G4Box("TrackerSiliBig_Solid", trackerSiliBigWidth / 2, trackerSiliBigHeight / 2, trackerSiliBigThickness / 2);

    G4LogicalVolume* trackerSiliLog0_0 = new G4LogicalVolume(trackerSiliBigBox, silicon, "TrackerSili_Logical_0_0");  // Chamber 1 -- 0th
    G4LogicalVolume* trackerSiliLog0_1 = new G4LogicalVolume(trackerSiliBigBox, silicon, "TrackerSili_Logical_0_1");  // Chamber 1 -- 1st
    G4LogicalVolume* trackerSiliLog1_0 = new G4LogicalVolume(trackerSiliBigBox, silicon, "TrackerSili_Logical_1_0");  // Chamber 2 -- 0th
    G4LogicalVolume* trackerSiliLog1_1 = new G4LogicalVolume(trackerSiliBigBox, silicon, "TrackerSili_Logical_1_1");  // Chamber 2 -- 1st
    trackerSiliLog0_0->SetVisAttributes(green);
    trackerSiliLog0_1->SetVisAttributes(green);
	trackerSiliLog1_0->SetVisAttributes(green);
    trackerSiliLog1_1->SetVisAttributes(green);

    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zChamber1+2.5*cm), trackerSiliLog0_0, "TrackerSili_0_0", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zChamber1-2.5*cm), trackerSiliLog0_1, "TrackerSili_0_1", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zChamber2+2.5*cm), trackerSiliLog1_0, "TrackerSili_1_0", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zChamber2-2.5*cm), trackerSiliLog1_1, "TrackerSili_1_1", worldLog, false, 0);
    
    // ============================================================================
    //                                CALORIMETER PLACEMENT
    // ============================================================================
    // Define the dimensions of the layers
    G4double caloLatWidth = 15. * cm;
    G4double caloActiveDepth = 2. * cm;
    G4double caloPassiveDepthSmall = 0.5 * cm;
    G4double caloPassiveDepthBig = 1. * cm;
    
    // Build the solids
    G4VSolid* caloActiveBox = new G4Box("PhActive_Solid", caloLatWidth / 2, caloLatWidth / 2, caloActiveDepth / 2);
    G4VSolid* caloPassiveSmallBox = new G4Box("PhPassiveSmall_Solid", caloLatWidth / 2, caloLatWidth / 2, caloPassiveDepthSmall / 2);
    G4VSolid* caloPassiveBigBox = new G4Box("PhPassiveBig_Solid", caloLatWidth / 2, caloLatWidth / 2, caloPassiveDepthBig / 2);
	
    // Build the logicals
    G4LogicalVolume* caloPhLogActive_00 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_00_Logical");
    G4LogicalVolume* caloPhLogPassive_00 = new G4LogicalVolume(caloPassiveSmallBox, lead, "PhPassive_00_Logical");
    
    G4LogicalVolume* caloPhLogActive_01 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_01_Logical");
    G4LogicalVolume* caloPhLogPassive_01 = new G4LogicalVolume(caloPassiveSmallBox, lead, "PhPassive_01_Logical");

    G4LogicalVolume* caloPhLogActive_02 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_02_Logical");
    G4LogicalVolume* caloPhLogPassive_02 = new G4LogicalVolume(caloPassiveSmallBox, lead, "PhPassive_02_Logical");
    
    G4LogicalVolume* caloPhLogActive_03 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_03_Logical");
    G4LogicalVolume* caloPhLogPassive_03 = new G4LogicalVolume(caloPassiveSmallBox, lead, "PhPassive_03_Logical");
    
    G4LogicalVolume* caloPhLogActive_04 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_04_Logical");
    G4LogicalVolume* caloPhLogPassive_04 = new G4LogicalVolume(caloPassiveSmallBox, lead, "PhPassive_04_Logical");

    G4LogicalVolume* caloPhLogActive_05 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_05_Logical");
    G4LogicalVolume* caloPhLogPassive_05 = new G4LogicalVolume(caloPassiveSmallBox, lead, "PhPassive_05_Logical");

    G4LogicalVolume* caloPhLogActive_06 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_06_Logical");
    G4LogicalVolume* caloPhLogPassive_06 = new G4LogicalVolume(caloPassiveSmallBox, lead, "PhPassive_06_Logical");
    
    G4LogicalVolume* caloPhLogActive_07 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_07_Logical");
    G4LogicalVolume* caloPhLogPassive_07 = new G4LogicalVolume(caloPassiveSmallBox, lead, "PhPassive_07_Logical");
    
    G4LogicalVolume* caloPhLogActive_08 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_08_Logical");
    G4LogicalVolume* caloPhLogPassive_08 = new G4LogicalVolume(caloPassiveSmallBox, lead, "PhPassive_08_Logical");
    
    G4LogicalVolume* caloPhLogActive_09 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_09_Logical");
    G4LogicalVolume* caloPhLogPassive_09 = new G4LogicalVolume(caloPassiveBigBox, lead, "PhPassive_09_Logical");
    
    G4LogicalVolume* caloPhLogActive_10 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_10_Logical");
    G4LogicalVolume* caloPhLogPassive_10 = new G4LogicalVolume(caloPassiveBigBox, lead, "PhPassive_10_Logical");
    
    G4LogicalVolume* caloPhLogActive_11 = new G4LogicalVolume(caloActiveBox, plastic, "PhActive_11_Logical");
    G4LogicalVolume* caloPhLogPassive_11 = new G4LogicalVolume(caloPassiveBigBox, lead, "PhPassive_11_Logical");
    
    caloPhLogActive_00->SetVisAttributes(cyan);
    caloPhLogPassive_00->SetVisAttributes(brown);
    caloPhLogActive_01->SetVisAttributes(cyan);
    caloPhLogPassive_01->SetVisAttributes(brown);
    caloPhLogActive_02->SetVisAttributes(cyan);
    caloPhLogPassive_02->SetVisAttributes(brown);
    caloPhLogActive_03->SetVisAttributes(cyan);
    caloPhLogPassive_03->SetVisAttributes(brown);
    caloPhLogActive_04->SetVisAttributes(cyan);
    caloPhLogPassive_04->SetVisAttributes(brown);
    caloPhLogActive_05->SetVisAttributes(cyan);
    caloPhLogPassive_05->SetVisAttributes(brown);
    caloPhLogActive_06->SetVisAttributes(cyan);
    caloPhLogPassive_06->SetVisAttributes(brown);
    caloPhLogActive_07->SetVisAttributes(cyan);
    caloPhLogPassive_07->SetVisAttributes(brown);
    caloPhLogActive_08->SetVisAttributes(cyan);
    caloPhLogPassive_08->SetVisAttributes(brown);
    caloPhLogActive_09->SetVisAttributes(cyan);
    caloPhLogPassive_09->SetVisAttributes(brown);
    caloPhLogActive_10->SetVisAttributes(cyan);
    caloPhLogPassive_10->SetVisAttributes(brown);
    caloPhLogActive_11->SetVisAttributes(cyan);
    caloPhLogPassive_11->SetVisAttributes(brown);

    // Center of the layers
    G4double zCenterActive_00  =  zPhCaloFront + (0*caloActiveDepth + 0*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_00 =  zPhCaloFront + (1*caloActiveDepth + 0*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthSmall/2; 
    G4double zCenterActive_01  =  zPhCaloFront + (1*caloActiveDepth + 1*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_01 =  zPhCaloFront + (2*caloActiveDepth + 1*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthSmall/2;
    G4double zCenterActive_02  =  zPhCaloFront + (2*caloActiveDepth + 2*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_02 =  zPhCaloFront + (3*caloActiveDepth + 2*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthSmall/2;
    G4double zCenterActive_03  =  zPhCaloFront + (3*caloActiveDepth + 3*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_03 =  zPhCaloFront + (4*caloActiveDepth + 3*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthSmall/2;
    G4double zCenterActive_04  =  zPhCaloFront + (4*caloActiveDepth + 4*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_04 =  zPhCaloFront + (5*caloActiveDepth + 4*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthSmall/2;
    G4double zCenterActive_05  =  zPhCaloFront + (5*caloActiveDepth + 5*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_05 =  zPhCaloFront + (6*caloActiveDepth + 5*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthSmall/2;
    G4double zCenterActive_06  =  zPhCaloFront + (6*caloActiveDepth + 6*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_06 =  zPhCaloFront + (7*caloActiveDepth + 6*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthSmall/2;
    G4double zCenterActive_07  =  zPhCaloFront + (7*caloActiveDepth + 7*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_07 =  zPhCaloFront + (8*caloActiveDepth + 7*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthSmall/2;
    G4double zCenterActive_08  =  zPhCaloFront + (8*caloActiveDepth + 8*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_08 =  zPhCaloFront + (9*caloActiveDepth + 8*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthSmall/2;
    
    // Note: the last three passive layers are bigger...
    G4double zCenterActive_09  =  zPhCaloFront + (9*caloActiveDepth + 9*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_09 =  zPhCaloFront + (10*caloActiveDepth + 9*caloPassiveDepthSmall + 0*caloPassiveDepthBig) + caloPassiveDepthBig/2;
    G4double zCenterActive_10  =  zPhCaloFront + (10*caloActiveDepth + 9*caloPassiveDepthSmall + 1*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_10 =  zPhCaloFront + (11*caloActiveDepth + 9*caloPassiveDepthSmall + 1*caloPassiveDepthBig) + caloPassiveDepthBig/2;
    G4double zCenterActive_11  =  zPhCaloFront + (11*caloActiveDepth + 9*caloPassiveDepthSmall + 2*caloPassiveDepthBig) + caloActiveDepth/2; 
    G4double zCenterPassive_11 =  zPhCaloFront + (12*caloActiveDepth + 9*caloPassiveDepthSmall + 2*caloPassiveDepthBig) + caloPassiveDepthBig/2; 

    // Placements
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_00),  caloPhLogActive_00,  "PhCalTestActive_00",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_00), caloPhLogPassive_00, "PhCalTestPassive_00", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_01),  caloPhLogActive_01,  "PhCalTestActive_01",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_01), caloPhLogPassive_01, "PhCalTestPassive_01", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_02),  caloPhLogActive_02,  "PhCalTestActive_02",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_02), caloPhLogPassive_02, "PhCalTestPassive_02", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_03),  caloPhLogActive_03,  "PhCalTestActive_03",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_03), caloPhLogPassive_03, "PhCalTestPassive_03", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_04),  caloPhLogActive_04,  "PhCalTestActive_04",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_04), caloPhLogPassive_04, "PhCalTestPassive_04", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_05),  caloPhLogActive_05,  "PhCalTestActive_05",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_05), caloPhLogPassive_05, "PhCalTestPassive_05", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_06),  caloPhLogActive_06,  "PhCalTestActive_06",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_06), caloPhLogPassive_06, "PhCalTestPassive_06", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_07),  caloPhLogActive_07,  "PhCalTestActive_07",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_07), caloPhLogPassive_07, "PhCalTestPassive_07", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_08),  caloPhLogActive_08,  "PhCalTestActive_08",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_08), caloPhLogPassive_08, "PhCalTestPassive_08", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_09),  caloPhLogActive_09,  "PhCalTestActive_09",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_09), caloPhLogPassive_09, "PhCalTestPassive_09", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_10),  caloPhLogActive_10,  "PhCalTestActive_10",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_10), caloPhLogPassive_10, "PhCalTestPassive_10", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterActive_11),  caloPhLogActive_11,  "PhCalTestActive_11",  worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zCenterPassive_11), caloPhLogPassive_11, "PhCalTestPassive_11", worldLog, false, 0);
    
	// ============================================================================
    //                                 CONCLUDE
    // ============================================================================
    // Print list of defined material
    G4cout << "-----" << G4endl;
    G4cout << "| DetectorConstruction.cc: material list" << G4endl;
    G4cout << *(G4Material::GetMaterialTable()) << G4endl;
    G4cout << "-----" << G4endl;
	
    return worldPhys;
}

// DetectorConstruction::ConstructSDandField, i.e. where the sensitive detectors and magnetic fields are implemented
void DetectorConstruction::ConstructSDandField()
{
    // ============================================================================
    //                                 Sensitive Detectors
    // ============================================================================
    // Load the sensitive detector manager
    G4SDManager* sdm = G4SDManager::GetSDMpointer();
    sdm->SetVerboseLevel(1);  // set sensitive detector manager verbosity here

    // SD: calorimeter
    VolumeEDepSD* gammaCalSDActive_00 = new VolumeEDepSD("PhCalTestActive_00_SD");
    SetSensitiveDetector("PhActive_00_Logical", gammaCalSDActive_00);
    sdm->AddNewDetector(gammaCalSDActive_00);

    VolumeEDepSD* gammaCalSDPassive_00 = new VolumeEDepSD("PhCalTestPassive_00_SD");
    SetSensitiveDetector("PhPassive_00_Logical", gammaCalSDPassive_00);
    sdm->AddNewDetector(gammaCalSDPassive_00);

    VolumeEDepSD* gammaCalSDActive_01 = new VolumeEDepSD("PhCalTestActive_01_SD");
    SetSensitiveDetector("PhActive_01_Logical", gammaCalSDActive_01);
    sdm->AddNewDetector(gammaCalSDActive_01);

    VolumeEDepSD* gammaCalSDPassive_01 = new VolumeEDepSD("PhCalTestPassive_01_SD");
    SetSensitiveDetector("PhPassive_01_Logical", gammaCalSDPassive_01);
    sdm->AddNewDetector(gammaCalSDPassive_01);

    VolumeEDepSD* gammaCalSDActive_02 = new VolumeEDepSD("PhCalTestActive_02_SD");
    SetSensitiveDetector("PhActive_02_Logical", gammaCalSDActive_02);
    sdm->AddNewDetector(gammaCalSDActive_02);

    VolumeEDepSD* gammaCalSDPassive_02 = new VolumeEDepSD("PhCalTestPassive_02_SD");
    SetSensitiveDetector("PhPassive_02_Logical", gammaCalSDPassive_02);
    sdm->AddNewDetector(gammaCalSDPassive_02);

    VolumeEDepSD* gammaCalSDActive_03 = new VolumeEDepSD("PhCalTestActive_03_SD");
    SetSensitiveDetector("PhActive_03_Logical", gammaCalSDActive_03);
    sdm->AddNewDetector(gammaCalSDActive_03);

    VolumeEDepSD* gammaCalSDPassive_03 = new VolumeEDepSD("PhCalTestPassive_03_SD");
    SetSensitiveDetector("PhPassive_03_Logical", gammaCalSDPassive_03);
    sdm->AddNewDetector(gammaCalSDPassive_03);

    VolumeEDepSD* gammaCalSDActive_04 = new VolumeEDepSD("PhCalTestActive_04_SD");
    SetSensitiveDetector("PhActive_04_Logical", gammaCalSDActive_04);
    sdm->AddNewDetector(gammaCalSDActive_04);

    VolumeEDepSD* gammaCalSDPassive_04 = new VolumeEDepSD("PhCalTestPassive_04_SD");
    SetSensitiveDetector("PhPassive_04_Logical", gammaCalSDPassive_04);
    sdm->AddNewDetector(gammaCalSDPassive_04);

    VolumeEDepSD* gammaCalSDActive_05 = new VolumeEDepSD("PhCalTestActive_05_SD");
    SetSensitiveDetector("PhActive_05_Logical", gammaCalSDActive_05);
    sdm->AddNewDetector(gammaCalSDActive_05);

    VolumeEDepSD* gammaCalSDPassive_05 = new VolumeEDepSD("PhCalTestPassive_05_SD");
    SetSensitiveDetector("PhPassive_05_Logical", gammaCalSDPassive_05);
    sdm->AddNewDetector(gammaCalSDPassive_05);

    VolumeEDepSD* gammaCalSDActive_06 = new VolumeEDepSD("PhCalTestActive_06_SD");
    SetSensitiveDetector("PhActive_06_Logical", gammaCalSDActive_06);
    sdm->AddNewDetector(gammaCalSDActive_06);

    VolumeEDepSD* gammaCalSDPassive_06 = new VolumeEDepSD("PhCalTestPassive_06_SD");
    SetSensitiveDetector("PhPassive_06_Logical", gammaCalSDPassive_06);
    sdm->AddNewDetector(gammaCalSDPassive_06);

    VolumeEDepSD* gammaCalSDActive_07 = new VolumeEDepSD("PhCalTestActive_07_SD");
    SetSensitiveDetector("PhActive_07_Logical", gammaCalSDActive_07);
    sdm->AddNewDetector(gammaCalSDActive_07);

    VolumeEDepSD* gammaCalSDPassive_07 = new VolumeEDepSD("PhCalTestPassive_07_SD");
    SetSensitiveDetector("PhPassive_07_Logical", gammaCalSDPassive_07);
    sdm->AddNewDetector(gammaCalSDPassive_07);

    VolumeEDepSD* gammaCalSDActive_08 = new VolumeEDepSD("PhCalTestActive_08_SD");
    SetSensitiveDetector("PhActive_08_Logical", gammaCalSDActive_08);
    sdm->AddNewDetector(gammaCalSDActive_08);

    VolumeEDepSD* gammaCalSDPassive_08 = new VolumeEDepSD("PhCalTestPassive_08_SD");
    SetSensitiveDetector("PhPassive_08_Logical", gammaCalSDPassive_08);
    sdm->AddNewDetector(gammaCalSDPassive_08);

    VolumeEDepSD* gammaCalSDActive_09 = new VolumeEDepSD("PhCalTestActive_09_SD");
    SetSensitiveDetector("PhActive_09_Logical", gammaCalSDActive_09);
    sdm->AddNewDetector(gammaCalSDActive_09);

    VolumeEDepSD* gammaCalSDPassive_09 = new VolumeEDepSD("PhCalTestPassive_09_SD");
    SetSensitiveDetector("PhPassive_09_Logical", gammaCalSDPassive_09);
    sdm->AddNewDetector(gammaCalSDPassive_09);

    VolumeEDepSD* gammaCalSDActive_10 = new VolumeEDepSD("PhCalTestActive_10_SD");
    SetSensitiveDetector("PhActive_10_Logical", gammaCalSDActive_10);
    sdm->AddNewDetector(gammaCalSDActive_10);

    VolumeEDepSD* gammaCalSDPassive_10 = new VolumeEDepSD("PhCalTestPassive_10_SD");
    SetSensitiveDetector("PhPassive_10_Logical", gammaCalSDPassive_10);
    sdm->AddNewDetector(gammaCalSDPassive_10);

    VolumeEDepSD* gammaCalSDActive_11 = new VolumeEDepSD("PhCalTestActive_11_SD");
    SetSensitiveDetector("PhActive_11_Logical", gammaCalSDActive_11);
    sdm->AddNewDetector(gammaCalSDActive_11);

    VolumeEDepSD* gammaCalSDPassive_11 = new VolumeEDepSD("PhCalTestPassive_11_SD");
    SetSensitiveDetector("PhPassive_11_Logical", gammaCalSDPassive_11);
    sdm->AddNewDetector(gammaCalSDPassive_11);

    // SD: crystal under test
    VolumeEDepSD* ChSD_22 = new VolumeEDepSD("Cherenkov_SD");
    SetSensitiveDetector("Ch_logical", ChSD_22);
    sdm->AddNewDetector(ChSD_22);

    // SD: Multiplicity Counter
    VolumeEDepSD* MC_SD = new VolumeEDepSD("MC_SD");
    SetSensitiveDetector("MC_logical", MC_SD);
    sdm->AddNewDetector(MC_SD);
	
    // SD: silicon trackers (1 sensitive detector per tracking plane)
    // (meaning 1 SD for each telescope and 2 SD for each chamber)
    VolumeTrackingSD* trackerSD0_0 = new VolumeTrackingSD("Tracker_SD_0_0");
    SetSensitiveDetector("TrackerSili_Logical_0_0", trackerSD0_0);
    sdm->AddNewDetector(trackerSD0_0);

    VolumeTrackingSD* trackerSD0_1 = new VolumeTrackingSD("Tracker_SD_0_1");
    SetSensitiveDetector("TrackerSili_Logical_0_1", trackerSD0_1);
    sdm->AddNewDetector(trackerSD0_1);

    VolumeTrackingSD* trackerSD1_0 = new VolumeTrackingSD("Tracker_SD_1_0");
    SetSensitiveDetector("TrackerSili_Logical_1_0", trackerSD1_0);
    sdm->AddNewDetector(trackerSD1_0);

    VolumeTrackingSD* trackerSD1_1 = new VolumeTrackingSD("Tracker_SD_1_1");
    SetSensitiveDetector("TrackerSili_Logical_1_1", trackerSD1_1);
    sdm->AddNewDetector(trackerSD1_1);
}