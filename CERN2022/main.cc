// Standard include and ifdef
#include "DetectorConstruction.hh"
#include "ActionInitialization.hh"

#include "G4RunManagerFactory.hh"
#include "G4SteppingVerbose.hh"
#include "G4UImanager.hh"
#include "FTFP_BERT.hh"
#include "G4FastSimulationPhysics.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

#include "Randomize.hh"

#include <G4MTRunManager.hh>
using RunManager = G4MTRunManager;

// Scoring
#include "G4RunManager.hh"
#include "G4ScoringManager.hh"

int main(int argc, char** argv)
{
    // Begin printout in a somewhat verbose mode
    cout << "-----" << endl;
    cout << "| main.cc: let's start!" << endl;
    cout << "-----" << endl;

    // Detect interactive mode (if no arguments) and define UI session
    //
    G4UIExecutive* ui = nullptr;
    if ( argc == 1 ) { ui = new G4UIExecutive(argc, argv); }

    // Use G4SteppingVerboseWithUnits
    G4int precision = 4;
    G4SteppingVerbose::UseBestUnit(precision);

    // load run manager
    auto runManager = new RunManager();
    runManager->SetVerboseLevel(0);  // <<< set run manager verbosity here

    // Activate command-based scorer
    G4ScoringManager::GetScoringManager();

    // Set mandatory initialization classes
    // Detector construction
    runManager->SetUserInitialization(new DetectorConstruction());
 
    // Physics list
    G4VModularPhysicsList* physicsList = new FTFP_BERT;

    physicsList->SetVerboseLevel(1);
    runManager->SetUserInitialization(physicsList);

    // User action initialization
    runManager->SetUserInitialization(new ActionInitialization());

    // Initialize visualization
    //
    G4VisManager* visManager = new G4VisExecutive;
    // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
    // G4VisManager* visManager = new G4VisExecutive("Quiet");
    visManager->Initialize();

    // Get the pointer to the User Interface manager
    G4UImanager* UImanager = G4UImanager::GetUIpointer();

    // Process macro or start UI session
    //
    if ( ! ui ) {
        // batch mode
        G4String command = "/control/execute ";
        G4String fileName = argv[1];
        UImanager->ApplyCommand(command+fileName);
    }
    else {
        // interactive mode
        UImanager->ApplyCommand("/control/execute init_vis.mac");
        ui->SessionStart();
        delete ui;
    }

    // Job termination
    // Free the store: user actions, physics_list and detector_description are
    // owned and deleted by the run manager, so they should not be deleted
    // in the main() program !

    delete visManager;
    delete runManager;

	
    cout << "-----" << endl;
    cout << "| main.cc: done, goodbye!" << endl;
    cout << "-----" << endl;

    return 0;
}
