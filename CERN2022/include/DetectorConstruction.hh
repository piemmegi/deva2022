#ifndef DETECTOR_CONSTRUCTION_HH
#define DETECTOR_CONSTRUCTION_HH

#include <G4SystemOfUnits.hh>
#include <G4VUserDetectorConstruction.hh>
#include <G4NistManager.hh>
#include <G4SDManager.hh>

using namespace std;

// class for logical volumes
class G4LogicalVolume;

// Class for G4regions
class G4Region;

// DetectorConstruction, i.e. the class with all the setup info (physical objects, detectors, magnetic fields)
class DetectorConstruction : public G4VUserDetectorConstruction
{
public:
    DetectorConstruction();
    G4VPhysicalVolume* Construct() override;
    void ConstructSDandField() override;

private:

};

#endif
