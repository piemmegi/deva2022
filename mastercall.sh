#! /usr/bin/bash

# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# The aim of this script is to call multiple times the "deva2022" simulation, 
#
# Syntax to call this script (supposing to be already in the "build" folder):
# $ ../mastercall.sh
#
# ==========================================================================================
#                                      INPUT PARAMETER
# ==========================================================================================
# Define the parameters for the execution of the multiple runs.
declare -a arr=("500" "1000" "2000" "4000") # [array] Energies to be tested (in MeV)
declare -a arr2=("1" "2")                   # [array] Number of particles to shoot at each event
total_stat=50000                            # [int] Total statistics acquired for each mode of operation
partial_stat=10000                          # [int] Number of events per single output file
printoutflag=250                            # [int] Flag for printout of the current number of events
ifVM='False'                                # [bool] Is this script running on a CERN VM?

# ==========================================================================================
#                                         SOURCING
# ==========================================================================================
# First of all, source all the required programmes to make the simulation work (i.e., 
# Geant4, C++ compiler, CMake, ROOT)
if [ $ifVM == 'True' ];
then
    # VM
    source /cvmfs/sft.cern.ch/lcg/contrib/gcc/9/x86_64-centos8/setup.sh
    source /cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/GNUMake-setup.sh
    source /cvmfs/sft.cern.ch/lcg/contrib/CMake/3.18.3/Linux-x86_64/setup.sh
    source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.28.00/x86_64-centos8-gcc85-opt/bin/thisroot.sh
else
    # Local (WLS)
    source ~/Geant4.11.0.2/geant4-11.0.2-install/bin/geant4.sh
    source ~/root/bin/thisroot.sh
fi

# Define the number of threads for multi-threaded runs (default is 4)
export G4FORCENUMBEROFTHREADS=4

# ==========================================================================================
#                                        SIMULATION
# ==========================================================================================
# Iterate over every energy to be tested and every particle multiplicity
for numparticles in "${arr2[@]}"; do
    for energy in "${arr[@]}"; do
        # Modify the source code of the simulation through a dedicated Python script
        python3 ../mastercall_texteditor.py "${total_stat}" "${partial_stat}" "${printoutflag}" "${energy}" "${numparticles}"
        
        # Clean the content possibly remaining in the CERN2022-build folder (with the exception of .gitkeep
        # and hypothetical other files)
        python3 ../build_cleaner.py

        # Make of the simulation
        if [ $ifVM == 'True' ];
        then
            # VM
            cmake "-DGeant4_DIR=/cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/lib64/Geant4-11.0.2/" "../CERN2022"
        else
            # Local (WLS)
            cmake "-DGeant4_DIR=~/Geant4.11.0.2/geant4-11.0.2-install/lib/Geant4-11.0.2/" "../CERN2022"
        fi
        make "-j4"

        # Call the simulation
        ./deva2022 macros/run_forphysics.mac

        # Merge the output files
        hadd out_data/outfile.root out_data/tbeamdata*.root

        # Move the output files in a dedicated folder, through a second Python script
        python3 ../mastercall_renamer.py "${energy}" "${numparticles}"
    done
done

# The end
echo "----------------------------------------"
echo "mastercall.sh, done!"
echo "----------------------------------------"