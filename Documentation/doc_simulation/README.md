# A guide on how does this simulation work

Guide written and edited by [P. Monti-Guarnieri](mailto:monti.guarnieri.p@gmail.com). 

Date of the last update of this guide: 2023/05/08.

## Table Of Contents

[[_TOC_]]

## Introduction

This code allows to simulate the energy deposit inside the Deva calorimeter (the experimental setup is shown [here](https://agenda.infn.it/event/16158/contributions/33129/attachments/23445/26650/muone_jul18_soldani_calorimeters.pdf)), with the experimental setup used in the STORM 2022 beamtest ([see here](https://docs.google.com/drawings/d/1Sgg2wQkdmHGgE4MfHCPVHCtLsiPe-IHwGdv-RA2XDE0/edit?usp=sharing) for the setup). Several quantities are recorded in the simulation, in two ways:
1. ROOT output file, where the data produced by all the Sensitive Detectors (SD) are saved. The detectors are: 
	- A Cherenkov detector
	- Two silicon microstrip detectors 
	- A segmented electromagnetic calorimeter, composed of plastic and lead layers and called **Deva**.

	The structure and content of the ROOT file is detailed below and can be checked also in the [RunAction constructor](CERN2022/src/RunAction.cc#L39).

2. CSV output file, which contains the scoring of the energy deposited in the calorimeter. Note that here the scoring is not performed with SDs, but rather through a user-defined meshgrid. The meshgrid is essentially a box with a user-defined extension and center, which features a multi-cell structure: the cells are equally spaced and their number is user-defined. Each cell records the total energy deposited inside it during **all** the simulation. This means that the meshgrid is optimal for measuring the *average* energy deposited in each cell, but not its distribution. The way to extract and analyze the data produced by the meshgrid is detailed below. The structure of the meshgrid is defined in the [macro file](CERN2022/macros/run_forphysics.mac#L26) and can be modified at will.

The simulation can be run either making the simulation and calling the single macros, or simply calling the `mastercall.sh` script from `CERN2022-build`. The difference is essentially one: the mastercall allows to split the total statistics available in several Runs. At the end of each Run, an output file is produced, while after all the Runs, the output files are merged in a single one. The mastercall script takes care also of a few other things:
- Modifying the source code in the required points.
- Sourcing the compiler, Geant4 and ROOT.
- Compiling the simulation code.
- Calling the simulation code.
- Merging the output files.
- Moving all the output files in the [Outputfiles](./Outputfiles) folder.

However, before calling the mastercall, two things should be done:
- Adjust the total and partial statistics to be acquired (plus some other things, such as if the script is running on a CERN VM or on a local WSL and also the partial statistics for the printout of a warning in the terminal). This can be done by modifying lines [15-18](./mastercall.sh#15) in the mastercall script.
- Check if the [`line_printoutflag`](./mastercall_texteditor.py#31) and [`line_run`](./mastercall_texteditor.py#36) variables point to the right locations in the source code. In particular:
	- [`line_printoutflag`] should point to something like `G4int NumofPrintProgress = 20;` in the [RunAction](CERN2022/src/RunAction.cc#28) file.
	- [`line_run`] should point to something like `/run/beamOn 100` in the [run_forphysics](CERN2022/macros/run_forphysics.mac#36) macro file.

Note that the variables here mentioned should already be pointing to the correct places in the source code: any modification should occur **if and only if** the source code of this simulation is modified.

## Output file structure

| Name of the ntuple   | Content                                               |
|:--------------------:|:-----------------------------------------------------:|
| NEvent               | Event number (ID)                                     |
| Tracker_NHit_X_0     | Number of particles crossing C1 (x)                   |
| Tracker_NHit_Y_0     | Number of particles crossing C1 (y)                   |
| Tracker_NHit_X_1     | Number of particles crossing C2 (x)                   |
| Tracker_NHit_Y_1     | Number of particles crossing C2 (y)                   |
| Tracker_X_0          | Hit position on the plane of C1 (x)                   |
| Tracker_Y_0          | Hit position on the plane of C1 (y)                   |
| Tracker_X_1          | Hit position on the plane of C2 (x)                   |
| Tracker_Y_1          | Hit position on the plane of C2 (y)                   |
| Deva_EDep_Active_00  | Energy deposited in the active layer n. 00 of Deva    |
| Deva_EDep_Passive_00 | Energy deposited in the passive layer n. 00 of Deva   |
| Deva_EDep_Active_01  | Energy deposited in the active layer n. 01 of Deva    |
| Deva_EDep_Passive_01 | Energy deposited in the passive layer n. 01 of Deva   |
| Deva_EDep_Active_02  | Energy deposited in the active layer n. 02 of Deva    |
| Deva_EDep_Passive_02 | Energy deposited in the passive layer n. 02 of Deva   |
| Deva_EDep_Active_03  | Energy deposited in the active layer n. 03 of Deva    |
| Deva_EDep_Passive_03 | Energy deposited in the passive layer n. 03 of Deva   |
| Deva_EDep_Active_04  | Energy deposited in the active layer n. 04 of Deva    |
| Deva_EDep_Passive_04 | Energy deposited in the passive layer n. 04 of Deva   |
| Deva_EDep_Active_05  | Energy deposited in the active layer n. 05 of Deva    |
| Deva_EDep_Passive_05 | Energy deposited in the passive layer n. 05 of Deva   |
| Deva_EDep_Active_06  | Energy deposited in the active layer n. 06 of Deva    |
| Deva_EDep_Passive_06 | Energy deposited in the passive layer n. 06 of Deva   |
| Deva_EDep_Active_07  | Energy deposited in the active layer n. 07 of Deva    |
| Deva_EDep_Passive_07 | Energy deposited in the passive layer n. 07 of Deva   |
| Deva_EDep_Active_08  | Energy deposited in the active layer n. 08 of Deva    |
| Deva_EDep_Passive_08 | Energy deposited in the passive layer n. 08 of Deva   |
| Deva_EDep_Active_09  | Energy deposited in the active layer n. 09 of Deva    |
| Deva_EDep_Passive_09 | Energy deposited in the passive layer n. 09 of Deva   |
| Deva_EDep_Active_10  | Energy deposited in the active layer n. 10 of Deva    |
| Deva_EDep_Passive_10 | Energy deposited in the passive layer n. 10 of Deva   |
| Deva_EDep_Active_11  | Energy deposited in the active layer n. 11 of Deva    |
| Deva_EDep_Passive_11 | Energy deposited in the passive layer n. 11 of Deva   |
| Cherenkov_EDep       | Energy deposited in the Cherenkov detector            |
| MC_EDep              | Energy deposited in the Multiplicity Counter          |

Notes:
- C1 and C2 identify the first and second silicon chamber. The first chamber is the closest to the source of the particle beam.
- The hit positions recorded by C1 and C2 are the average of all the positions where any ionizing particle left an energy larger than 50 keV, in a given event. If more particles passed through the detector in the event, all of them were considered in the calculation of the x/y hit position.
- All the hit positions are measured in cm, while the deposited energies are measured in GeV.


## Sample code for accessing the content of the output files

The data contained in the ROOT file (saved in the `Outputfiles` folder after the call to `mastercall.sh`) can be accessed with something like:
```python
# Import the necessary modules
import uproot
import numpy as np

# Open the file
filename = './Outputfiles/tbeamdata.root'
file = uproot.open(filename)['outData']

# View the keys
for el in file.keys():
	print(keys)

# Extract a variable
Nevents = np.array(file['NEvent'])
```

The data produced by the meshgrid (saved in the `Outputfiles` folder after the call to `mastercall.sh`) are formatted as a table, namely a CSV file. Note that this is simply the default for anyone using the UI commands in the macro files, without writing their own classed for producing the output file. In the CSV file there are three header columns. The first provides the name of the mesh used, while the second described the name of the scorer chosen (in our care, this is the deposited energy, but there could be also other alternatives). The third row simply states the name of the variables which will be put in all the following rows. At the moment, each of such rows include six informations:
- The first three provide the identifier of the meshgrid cell, whose output is given in the row (1 row = 1 cell in the meshgrid). The identifier (0,0,0) is the corner of the meshgrid with the lowest x,y,z values, thus this is **not** the center.
- The sum of the energy deposited during **ALL** the events in the given cell of the meshgrid. **Note**: the energy is measured, by default, in MeV.
- The sum of the square of the energy deposited during **ALL** the events in the given cell of the meshgrid. **Note**: the energy is measured, by default, in MeV.
- The number of events considered in the calculation of the other cells.

This means essentially that the **average** energy deposited in each cell (and the corresponding error) can be computed with a code such as the following:
```python
# Open the file
filename = './Outputfiles/meshdata.csv'
idX,idY,idZ,sumEntries,sumSquareEntries,Numevents = np.loadtxt(file, delimiter=',',skiprows = 3, unpack=True)

# Average values and sigmas
averages = sumEntries / Numevents
sigmas = np.sqrt( (sumSquareEntries / Numevents) - averages**2) / np.sqrt(Numevents)

# Total energy deposited on average in the calorimeter
Etot = np.sum(averages)
```

## Useful links
If you want to know more about the meshgrids:
- [The official Geant4 guide for application developers](http://lmu.web.psi.ch/docu/manuals/software_manuals/Geant4/Geant4_BookForAppliDev.pdf) (see section 4.8.2)
- [A more general presentation on the scoring methods](https://agenda.infn.it/event/9689/contributions/81045/attachments/58639/69143/Kernel_interaction_-_Part_2.pdf). A similar presentation is given [here](https://indico.cern.ch/event/746466/contributions/3345729/attachments/1833373/3003047/Scoring.pdf)
- [A good reference example for using UI commands in scoring methods](https://gitlab.cern.ch/geant4/geant4/-/blob/master/examples/extended/runAndEvent/RE03)
